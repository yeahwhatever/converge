#!/usr/bin/ruby -w

require "csv"

$debug = true

file = "harness_list.csv"

n = 0 
host = "https://www.google.com"

programs = [ 
	{:name => 'wget', :invoke => 'wget %s -O /dev/null', :good_ret => 0, :bad_ret => 5}
]

programs2 = CSV.read(file, {:skip_blanks => true, :quote_char => "\x00"})

#to see what the output looks like
print programs2
print "\n"

total_time = 0 

programs.each do |p| 
	n += 1

	cmd = sprintf(p[:invoke], host)

	avg = total_time/n
	eta = (programs.count - n) * avg 

	puts "[+] Starting #{p[:name]}:\n"
	     "\t(#{n}/#{programs.count})"
	     "\tAvg: #{avg} Total: #{total_time} ETA: #{eta}\n"

	puts "[*] #{p[:name]} starting at " + Time.now.strftime("%I:%M%p") 

	start_time = Time.now.to_i
	output = `#{cmd}`
	end_time = Time.now.to_i

	runtime = end_time - start_time
	total_time += runtime

	status = 'unknown'

	status = 'good' if $?.exitstatus == p[:good_ret]
	status = 'bad' if $?.exitstatus == p[:bad_ret]

	puts "[*] #{p[:name]} ending at " + Time.now.strftime("%I:%M%p")
	puts "[+] Total time for #{p[:name]}: #{runtime}"
	puts "[data] #{p[:name]},#{runtime},#{status}"

	if $debug
		puts "[*] #{p[:name]} output begin:"
		puts output 
		puts "[*] #{p[:name]} output end."
	end 
end
