<?php

global $config, $db, $userdb, $notaries, $debug;
$debug = true;

function main($argc, $argv)
{
    global $debug;
    
    date_default_timezone_set('America/Los_Angeles'); //FOR CONVENIENCE

    init();

    if ($argc < 3) exit(1);

    $host = $argv[1];
    $port = $argv[2];

    $fingerprint = (isset($argv[3]) ? 
        strtoupper($argv[3]) : get_fingerprint($host, $port));

    if ($debug) 
        echo "[+] Notary lookup: ${host}:${port} - ${fingerprint}" . PHP_EOL;

    // Perform lookup    
    $result = notarize($host, $port, $fingerprint);

    // We're golden
    if (result) exit(0);

    // We're fucked
    exit(1);
}


function init()
{
    global $config, $db, $userdb, $notaries;

    $config_path = '/usr/local/etc/converge/converge.config';

    // Load config
    $file   = file_get_contents($config_path ,'r');
    $config = json_decode($file);

    // Set up the dbs for caching...
    $db     = new SQLite3($config->{'db'});
    $userdb = new SQLite3($_ENV['HOME'] . '/' . $config->{'userdb'});

    // Read our enabled notaries
    $notaries_dir = $config->{'notaries-dir'};

    if ($handle = opendir($notaries_dir)) {
        
        while (false !== ($entry = readdir($handle))) {
            
            if ($entry != '.' && $entry != '..') {
                
                $bundle_path = $notaries_dir . $entry;
                $data = file_get_contents($bundle_path ,'r');
                $notaries[] = json_decode($data);
            }
        }
        closedir($handle);
    }
}


// Performs local x509 retrieval
function get_fingerprint($host, $port)
{
    // Open up our connection
    $ssl_context = stream_context_create(array('ssl' =>
        array('capture_peer_cert' => true)));
    
    $ssl_socket = stream_socket_client(
        'ssl://' . $host . ':' . $port, $errno, $errstr, 30,
        STREAM_CLIENT_CONNECT, $ssl_context);
    
    $context = stream_context_get_params($ssl_socket);

    // Get out x509 cert
    $cert = $context['options']['ssl']['peer_certificate'];

    openssl_x509_export($cert, $pem_cert);

    $der_cert = pem2der($pem_cert);

    $sha1 = strtoupper(join(str_split(sha1($der_cert), 2), ':'));

    return $sha1;
}


function notarize($host, $port, $fingerprint) 
{
    global $debug, $config;
    
    // First, check our local cache
    $result = cache_check($host, $port, $fingerprint);

    if ($result) {
        if ($debug)
            echo "[+] Cache hit: ${host}:${port} - ${fingerprint}" . PHP_EOL;	
        return true;
    }
    $result = remote_check($host, $port, $fingerprint);

    cache_update($result, $host, $port);

    // Count success/failures
    $good = $bad = 0;
    foreach ($result as $value) {
        if ($value[0])
            $good++;
        else
            $bad++;
    }
    
    if ($debug)
        echo "[+] Notary hits: $good\n[+] Notary misses: $bad" . PHP_EOL;

    if ($config->{'notary-agreement'} == 'consensus') {
        if ($bad)
            return false;
    }
    elseif ($config->{'notary-agreement'} == 'majority') {
        if ($good > $bad)
            return true;
    }   
    else
        return false;
}


function cache_check($host, $port, $fingerprint)
{
    global $db, $userdb;
    
    return (!_cache_check($host, $port, $fingerprint, $db) == 0 OR
            !_cache_check($host, $port, $fingerprint, $userdb) == 0);
}


function _cache_check($host, $port, $fingerprint, &$db)
{
    $timestamp = time() - 60*60*25*7;
    
    $stmt = 'SELECT COUNT(*) FROM fingerprints ' .
            'WHERE location = ? AND fingerprint = ? AND timestamp > ?';
    $stmt = $db->prepare($stmt);
    
    $stmt->bindValue(1, "$host:$port", SQLITE3_TEXT);
    $stmt->bindValue(2, $fingerprint, SQLITE3_TEXT);
    $stmt->bindValue(3, $timestamp, SQLITE3_INTEGER);
    
    $result = $stmt->execute();
    $result = $result->fetchArray(SQLITE3_NUM);
    $result = $result[0];

    return $result;
}


function cache_update($result, $host, $port)
{
    global $config, $db, $userdb;

    _cache_update($result, $host, $port, $userdb);

    if (is_writable($config->{'db'}))
        _cache_update($result, $host, $port, $db);
}


function _cache_update($result, $host, $port, &$db)
{
    foreach ($result as $r) {

        if ($r[0]) {
            
            $data = $r[1]->{'fingerprintList'};

            foreach ($data as $d) {
                $print = $d->{'fingerprint'};
                
                $stmt = 'INSERT OR IGNORE INTO fingerprints (location, fingerprint)' .
                        'VALUES (?, ?)';
                $stmt = $db->prepare($stmt);        
                
                $stmt->bindValue(1, "$host:$port", SQLITE3_TEXT);
                $stmt->bindValue(2, $print, SQLITE3_TEXT);
                
                $stmt->execute();
               
                $timestamp = time();
                
                $stmt = 'UPDATE fingerprints SET timestamp = ?' .
                        'WHERE location = ?' .
                        'AND fingerprint = ?';
                $stmt = $db->prepare($stmt);
                
                $stmt->bindValue(1, $timestamp, SQLITE3_INTEGER);
                $stmt->bindValue(2, "$host:$port", SQLITE3_TEXT);
                $stmt->bindValue(3, $print, SQLITE3_TEXT);
                
                $stmt->execute();
            }
        }          
    }
}


function remote_check($remote_host, $remote_port, $fingerprint)
{
    global $notaries;
    $requests = $results = $certs = $curl_results = array();

    foreach ($notaries as $notary) {
        $hosts = $notary->{'hosts'};
       
        foreach ((array)$hosts as $host) {
            $url = 'https://' . $host->{'host'} . ':' . strval($host->{'ssl_port'}) .
                   '/target/' . $remote_host . '+' . strval($remote_port);

            $cert = $host->{'certificate'};
            
            // Get cURL handle
            $curl_handle = curl_init();
            
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl_handle, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL            => $url,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POST           => 1,
                CURLOPT_POSTFIELDS     => 'fingerprint=' . $fingerprint
            ));

            array_push($requests, $curl_handle);

            // Keep a map [url => cert] for later use
            $certs[$url] = $cert;
        }
    }
    
    // Create the multiple cURL handle      
    $multi_handle = curl_multi_init();
    
    foreach ($requests as $handle) 
        curl_multi_add_handle($multi_handle, $handle);

    $running = null;
    
    do {
        curl_multi_exec($multi_handle, $running);
    } while ($running > 0);

    // Get the result and save it in the result array
    foreach ($requests as $key => $handle) {
        $url = curl_getinfo($handle, CURLINFO_EFFECTIVE_URL);
        $http_code = curl_getinfo($handle,CURLINFO_HTTP_CODE);
       
        // Only 200 is valid
        if($http_code != 200){
            markit_zero($results, $url);
            continue;
        }
        $curl_results[$key]['url']  = $url;
        $curl_results[$key]['cert'] = $certs[$url];
        $curl_results[$key]['data'] = curl_multi_getcontent($handle);
    }

    // Close all the connections
    foreach ($requests as $handle) {
        curl_multi_remove_handle($multi_handle, $handle);
    }
    curl_multi_close($multi_handle); 
        
    foreach ($curl_results as $key) {
        $response = $key['data'];
        $cert     = $key['cert'];
        $url      = $key['url'];

        $data = json_decode($response);
        $sig  = base64_decode($data->{'signature'});
        unset($data->{'signature'});
            
        $message = json_encode($data);
        $message = str_replace('":', '": ', str_replace(',', ', ', $message));
               
        $pkey = openssl_pkey_get_public($cert);

        if (openssl_verify($message, $sig, $pkey, OPENSSL_ALGO_SHA1))
            markit_good($results, $url, $data);
        else 
            markit_zero($results, $url);
    }
    return $results;
}


function markit_good(&$results, $url, $response)
{
    $results[$url] = array(true, $response);
}


function markit_zero(&$results, $url)
{
    $results[$url] = array(false);
}


function pem2der($pem_data)
{
    $begin = "CERTIFICATE-----";
    $end   = "-----END";
    
    $pem_data = substr($pem_data, strpos($pem_data, $begin)+strlen($begin));   
    $pem_data = substr($pem_data, 0, strpos($pem_data, $end));
    
    $der = base64_decode($pem_data);
    return $der;
}


main($argc, $argv);

?>
