#!/usr/bin/perl

use DBI;
use JSON::XS;
use IO::Socket::SSL;
use Net::SSLeay;
use Crypt::OpenSSL::X509;
use Crypt::OpenSSL::RSA;
use MIME::Base64;
use Digest::SHA1;
use LWP::UserAgent;
use Parallel::ForkManager;
use strict;
use warnings;

#Globals
our $config;
our $db = 0;
our $userdb = 0;
our @notaries;

sub main{
	#initialize
    init();

    my $host = $_[0];
    my $port = $_[1];
	my $fingerprint = $_[2];

	if (!$fingerprint) {
    	$fingerprint = get_fingerprint($host, $port);
	}

    my $result = notarize($host, $port, $fingerprint);
    
    if ($result){
        exit 0;
    } else {
        exit 1;
    }
}
 
sub init{
    my $config_path = '/usr/local/etc/converge/converge.config';
    # print $config_path;

    my $data;
    open(my $fh, '<', $config_path) or die "cannot open file $config_path";
    {
        local $/;
        $data = <$fh>;
    }
    close($fh);
    ##TEST###
    #print $data;

    #load JSON into hash tabe  
    $config = decode_json $data;

    $db = DBI->connect("dbi:SQLite:dbname=$config->{'db'}","","") or die "cannot connect to db";
    
    $userdb = DBI->connect("dbi:SQLite:dbname=$ENV{'HOME'}/$config->{'userdb'}","","") or die "fail";
    
    opendir(DH, $config->{'notaries-dir'});
    my @files = readdir(DH);
    closedir(DH);

    foreach my $file (@files){
        # skip . and ..
        next if($file =~ /^\.$/);
        next if($file =~ /^\.\.$/);

        my $bundle_path = $config->{'notaries-dir'}.$file;

        open($fh,$bundle_path);
        $data = <$fh>;

        my $temp = decode_json($data);
        push(@notaries, $temp);
    }
}

sub get_fingerprint{
    my $tcp_client = IO::Socket::SSL->new("$_[0]:$_[1]");

    my $cert = Net::SSLeay::PEM_get_string_X509($tcp_client->peer_certificate());

    #Remove "-----BEGIN CERTIFICATE-----" and "-----END CERTIFICATE-----"
    my $junk = substr($cert,0,27,"");
    my $junk2 = substr($cert,-26,26,"");

    #Get certificate in DER binary format
    my $cert_der = decode_base64($cert);

    my $fingerprint = Digest::SHA1::sha1_hex($cert_der);

    $tcp_client->close();

    #all caps
    $fingerprint = uc $fingerprint;
    
    #format fingerprint for convergence notary
    my $j = 2;
    for (my $i=0; $i < 19; $i++){
        substr($fingerprint, $j, 0, ":");
        $j += 3;
    }

    #TEST
    #print $cert."\n";
    #print $cert_der."\n";
    print $fingerprint."\n";
    return $fingerprint;
}

sub notarize{
    
    my $host = $_[0]; 
    my $port = $_[1];
    my $fingerprint = $_[2];

    my $result = cache_check($host, $port, $fingerprint);

    if ($result){
        print "cache hit\n";
        return 1;
    } 
    print "cache miss\n";
    $result = remote_check($host, $port, $fingerprint);
    
    cache_update($result, $host, $port);

    my $good = 0;
    my $bad = 0;

    for my $i ( values %{ $result }){
        if (@{ $i }[0]){
            $good += 1;
        } else {
            $bad += 1;
        }
    }
    #DEBUG
    print "[+] Notary hits: ".$good."\n"."[+] Notary misses: ".$bad."\n";

    if ($config->{'notary-agreement'} eq 'consensus'){
        return 0 if $bad;
    } elsif ($config->{'notary-agreement'} eq 'majority'){
        return 1 if ($good > $bad);
    } else {
        return 0
    }
}

sub cache_check{
    my $host = $_[0]; 
    my $port = $_[1];
    my $fingerprint = $_[2];

    my $query_handle = $userdb->prepare('SELECT COUNT(*) FROM fingerprints WHERE location = ? AND fingerprint = ? AND timestamp > ?') or die $DBI::errstr;
    
    my $time = time;
    my $rv = $query_handle->execute($host.':'.$port, $fingerprint, $time - (60*60*25*7));

    my $number_of_rows = $query_handle->fetchrow_arrayref;

    return ${ $number_of_rows }[0];
}

sub cache_update{
    my $results = $_[0];
    my $host = $_[1];
    my $port = $_[2];
    my $data;
    foreach my $value ( values %{ $results } ){
        if (@{ $value }[0]){
            $data = @{ $value }[1]->{'fingerprintList'};

            foreach my $d (@{ $data }){
                my $print = $d->{'fingerprint'};
                my $query_handle = $userdb->prepare('INSERT OR IGNORE INTO fingerprints (location, fingerprint) VALUES (?, ?)');
                my $rv = $query_handle->execute($host.':'.$port, $print);
                my $time = time;
                $query_handle = $userdb->prepare('UPDATE fingerprints SET timestamp = ? WHERE location = ? AND fingerprint = ?');
                $rv = $query_handle->execute( $time,$host.':'.$port, $print);
            }
            #last;
        }
    }
}

sub remote_check{
    my $remote_host = $_[0];
    my $remote_port = $_[1];
    my $fingerprint = $_[2];

    my @requests;
    my $results = {};

    for my $notary (@notaries){
        # $notary->{'hosts'} => array ref
        my @hosts = @{ $notary->{'hosts'}};
        
        for my $host (@hosts){
            
            my $url = 'https://'.$host->{'host'}.':'.$host->{'ssl_port'}.'/target/'.
                $remote_host.'+'.$remote_port;

            my $cert = Crypt::OpenSSL::X509->new_from_string($host->{'certificate'});
            my $pub_key = $cert->pubkey;

            #my $ua = LWP::UserAgent->new;
            my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0 });
            my $req = HTTP::Request->new(POST => $url);

            $req->content_type('application/x-www-form-urlencoded');
            $req->content('fingerprint='.$fingerprint);

            my $res = $ua->request($req);
            my $data;
            
            if ($res->is_success) {
                
                #need to do some string magic because JSON::XS is a piece of Shit
                my $index = index($res->decoded_content, ', "signature":',0);
                my $message = substr($res->decoded_content,0,$index);
                $message = $message.'}';

                $data = decode_json($res->decoded_content);
                
                #pull the signature out of the responce
                my $sig = decode_base64($data->{'signature'});
                delete $data->{'signature'};

                my $rsa = Crypt::OpenSSL::RSA->new_public_key($pub_key);

                if ($rsa->verify($message, $sig)){
                    print "success\n";
                    markit_good($results, $url, $data); 
                } 
            }
            else {
                print "no response\n";
                markit_zero($results, $url);
            }
        }
    }
    return $results;
}

sub markit_good{
    my $results = $_[0];
    my $url = $_[1];
    my $response = $_[2];
    my $array_ref = [1, $response];

    $results->{ $url } = $array_ref;
}
sub markit_zero{
    my $results = $_[0];
    my $url = $_[1];
    $results->{ $url } = [0]; 
}

main($ARGV[0], $ARGV[1]);
