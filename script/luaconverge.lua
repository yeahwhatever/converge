#!/usr/local/bin/lua

local fs = require('luarocks.fs')
local json = require('json')
local sqlite3 = require('lsqlite3')
local curl = require('cURL')
local base64 = require('base64')
local crypto = require('crypto')
--local socket = require('socket')
--local ssl = require('ssl')

-- Global Vars
config, notaries = {}, {}
db, userdb = nil, nil

debug = true


function main()
  -- Initialize
  init()

  -- Bail if we have wrong number of args
  if #arg < 2 then os.exit(1) end

  local host = arg[1]
  local port = arg[2]
  local fingerprint = (arg[3] or get_fingerprint(host, port)):upper()

  if debug then 
    print(('[+] Notary lookup: %s:%s - %s'):format(host, port, fingerprint))
  end

  local result = notarize(host, port, fingerprint)

  -- We're golden
  if result then os.exit(0) end

  -- We're fucked
  os.exit(1)
end


function init()
  local config_path = '/usr/local/etc/converge/converge.config'

  -- Load config
  local file = io.open(config_path, 'r')
  local data = file:read('*all')
  file:close()
  config = json.decode(data)

  -- Set up the dbs for caching...
  db = sqlite3.open(config['db'])
  userdb = sqlite3.open(os.getenv('HOME') .. '/' .. config['userdb'])

  -- Read our enabled notaries
  local bundle_path
  for _,fname in pairs(fs.list_dir(config['notaries-dir'])) do
    if not (fname == '.' or fname == '..') then
      bundle_path = config['notaries-dir'] .. fname
      file = io.open(bundle_path, 'r')
      data = file:read('*all')
      file:close()
      data = json.decode(data)
      table.insert(notaries, data)
    end
  end
end


function get_fingerprint(host, port)
  local cmd = io.popen(('echo |\ ' ..
                'openssl s_client -connect %s:%s 2>&1 |\ ' ..
                'sed -ne "/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p" |\ ' ..
                'openssl x509 -noout -sha1 -fingerprint'):format(host, port))
  local res = cmd:read('*a')
  cmd:close()
  local fingerprint = res:sub(res:find('=') +1, -1)
  return (fingerprint:gsub('^%s*(.-)%s*$', '%1'))

 -- local params = {
 --   mode = "client",
 --   protocol = "sslv23",
 --   verify = "none",
 --   options = "all",
 -- }
 -- 
 -- local conn = socket.tcp()
 -- conn:connect(host, port)
 -- conn = ssl.wrap(conn, params)
 -- conn:dohandshake()
 -- conn:send("GET / HTTP/1.1\n\n")
 -- local cert = conn:getpeercertificate()
 -- local pem = cert:pem()
 -- conn:close()
 -- local der = pem_to_der(pem)
 -- local fprint = crypto.digest('sha1', der)
 -- 
 -- return (fingerprint:gsub('^%s*(.-)%s*$', '%1'))
end


function cache_check(host, port, fingerprint)
  return _cache_check(host, port, fingerprint, db) or 
         _cache_check(host, port, fingerprint, userdb)
end


function _cache_check(host, port, fingerprint, db)
  local stmt = db:prepare('SELECT COUNT(*) FROM fingerprints ' ..
                    'WHERE location = ? AND fingerprint = ? ' ..
                    'AND timestamp > ?') 

  stmt:bind_values(('%s:%s'):format(host, port), fingerprint, 
                  (os.time() - 60*60*25*7))
  
  local count
  for row in stmt:rows() do
    count = row[1]
  end

  return count
end


function cache_update(result, host, port)
  _cache_update(result, host, port, userdb)

  -- If we can update the global cache, do it
  if fs.is_writable(config['db']) then
    _cache_update(result, host, port, db)
  end
end


function _cache_update(result, host, port, db)
  local data, fprint, stmt
  for _,r in pairs(result) do
    if r[1] then
      data = r[2]['fingerprintList']
      
      for _,d in pairs(data) do
        fprint = d['fingerprint']
        stmt = db:prepare('INSERT OR IGNORE INTO fingerprints ' ..
                          '(location, fingerprint) VALUES (?, ?)')
        stmt:bind_values(('%s:%s'):format(host, port), fprint)
        stmt:step()
        stmt:reset()
        
        stmt = db:prepare('UPDATE fingerprints SET timestamp = ? ' ..
                          'WHERE location = ? AND fingerprint = ?')
        stmt:bind_values(os.time(), ('%s:%s'):format(host, port), fprint)
        stmt:step()
        stmt:reset()
      end
    end
  end
end


function remote_check(remote_host, remote_port, fingerprint)
  -- TODO implement parallel requests

  local url, cert
  local hosts, certs, results = {}, {}, {}

  for _,notary in pairs(notaries) do
    hosts = notary['hosts']

    -- Loop over all hosts in a notary
    for _,host in pairs(hosts) do
      -- Build the notary request url
      url = ('https://%s:%s/target/%s+%s'):format(host['host'], host['ssl_port'],
              remote_host, remote_port)
      cert = host['certificate']
      certs[url] = cert
    end
  end

  -- NO need for two loops since multi_handle doesn't work

  local curl_handle, result, code, data, sig, message, file, verify, pubkey
  --multi_handle = curl.multi_init()
  for url,_ in pairs(certs) do
    curl_handle = curl.easy_init()
    curl_handle:setopt_url(url)
    curl_handle:setopt_post(1)
    curl_handle:setopt_postfields(('fingerprint=%s'):format(fingerprint))
    curl_handle:setopt_ssl_verifypeer(0)
    --curl_handle:setopt_verbose(1)
    --multi_handle:add_handle(curl_handle)
    
    if not pcall(curl_handle.perform, curl_handle, {
      writefunction=function(str)
        result = str
      end, 
      headerfunction=function(str)
        if str:find('200 OK') then code = 1 end
      end
    }) then markit_zero(results, url) end
  
    -- If connection fails
    if not code then
      markit_zero(results, url)
    else 
      code = nil 
      data = json.decode(result)
      sig = base64.decode(data['signature'])
      data['signature'] = nil
      
      -- message = json.encode(data) -- ERROR; table elements has no order
      -- message = (message:gsub('":', '": ')):gsub(',', ', ')
      message = result:sub(0, result:find('], "signature"'))..'}'
      
      cert = certs[url]
      file = io.open('cert.pem', 'w')
      file:write(cert)
      file:close()

      os.execute('openssl x509 -in cert.pem -pubkey -noout > key.pem')

      file = io.open('sig', 'w')
      file:write(sig)
      file:close()
      file = io.open('data', 'w')
      file:write(message)
      file:close()

      verify = io.popen('openssl dgst -sha1 -verify key.pem ' ..
                        '-signature sig data'):read('*a')

      --pubkey = io.popen('openssl x509 -in cert.pem -pubkey -noout'):read('*a')
      --pubkey = crypto.pkey.from_pem(pubkey)
      --verify = crypto.verify('sha1', message, sig, pubkey)

      os.remove('cert.pem')
      os.remove('key.pem')
      os.remove('sig')
      os.remove('data')
      
      if verify:find('OK') then 
        markit_good(results, url, data)
      else
        markit_zero(results, url)
      end
    end
  end
  return results
end


function notarize(host, port, fingerprint)
  -- First, check our local cache
  local result = cache_check(host, port, fingerprint)

  -- Cache hit, we're happy
  if result > 0 then
    if debug then 
      print(('[+] Cache hit: %s:%s - %s'):format(host, port, fingerprint))
    end
    return true
  end

  -- Cache miss, perform remote check
  result = remote_check(host, port, fingerprint)

  -- Update our caches
  cache_update(result, host, port)

  -- Count success/failures
  local good, bad = 0,0
  for _,value in pairs(result) do
    if value[1] then
      good = good + 1 
    else
      bad = bad + 1 
    end
  end
  
  if debug then
    print(('[+] Notary hits: %d\n[+] Notary misses: %d'):format(good, bad))
  end

  local agreement = config['notary-agreement']
  if agreement == 'consensus' then
    if bad > 0 then return false end

  elseif agreement == 'majority' then
    if good > bad then return true end

  else
    return false
  end
end


function markit_good(results, url, response)
  results[url] = {true, response}
end


function markit_zero(results, url)
  results[url] = {false}
end


function pem_to_der(pem)
  local begin = 'CERTIFICATE-----\n'
  local _end = '\n-----END'
  local _,b = pem:find(begin)
  local e = pem:find(_end)
  local der = pem:sub(b+1, e-1)
  der = base64.decode(der)
  return der
end


main()

