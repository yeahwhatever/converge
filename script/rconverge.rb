#!/usr/bin/ruby 

require 'json/pure'
require 'socket'
require 'openssl'
require 'sqlite3'
require 'base64'

require 'typhoeus'

# TODO
# - Add ~/.converge support

# Global Vars
$config = {}
$db = nil
$userdb = nil
$notaries = []

$debug = true

# Entry/exit point. Here we go!
def main()
	# Initialize
	init()

	# Bail if we have wrong number of args
	exit 1 unless ARGV.length > 1

	host = ARGV[0]
	port = ARGV[1]
	fingerprint = ARGV[2]

	# Perform local lookup of fingerprint
	if fingerprint
		fingerprint = fingerprint.upcase
	else
		fingerprint = get_fingerprint(host, port)
	end

	# Perform lookup
	puts "[+] Notary lookup: #{host}:#{port} - #{fingerprint}" if $debug
	result = notarize(host, port, fingerprint)

	# We're golden
	exit 0 if result
	
	# We're fucked
	exit 1
end

# Set up global vars
# TODO parse command line options
def init()
	config_path = '/usr/local/etc/converge/converge.config'
	
	# Load config
	data = File.open(config_path, 'r') {|f| f.read }
	$config = JSON.parse data

	# Set up the dbs for caching...
	$db = SQLite3::Database.new($config['db'])
	$userdb = SQLite3::Database.new(ENV['HOME'] +'/'+ $config['userdb'])

	# Read our enabled notaries
	Dir.foreach($config['notaries-dir']) do |f|
		next if f == '.' or f == '..'

		bundle_path = $config['notaries-dir'] + f
		data = File.open(bundle_path, 'r') {|d| d.read }

		$notaries << JSON.parse(data)
	end
end

# Performs local x509 retrieval
def get_fingerprint(host, port)
	# Open up our connection
	tcp_client = TCPSocket.new(host, port)

	ssl_context = OpenSSL::SSL::SSLContext.new

	ssl_socket = OpenSSL::SSL::SSLSocket.new(tcp_client, ssl_context)
	ssl_socket.sync_close = true
	ssl_socket.connect

	# Get out x509 cert
	cert = ssl_socket.peer_cert

	# Clean up
	ssl_socket.sysclose

	sha1 = OpenSSL::Digest::SHA1.new
	fingerprint = sha1.hexdigest(cert.to_der)
	
	# Add colons as per convergence spec, also it must be upper case for some reason
	return fingerprint.scan(/.{2}/).join(':').upcase.encode('UTF-8')
end


def notarize(host, port, fingerprint)
	# First, check our local cache
	result = cache_check(host, port, fingerprint)

	# Cache hit, we're happy
	if result
		puts "[+] Cache hit: #{host}:#{port} - #{fingerprint}" if $debug
		return true
	end

	# Cache miss, perform remote check	
	result = remote_check(host, port, fingerprint)

	# Update our caches
	cache_update(result, host, port)

	# Count success/failures
	good = result.values.count {|x| x[0]}
	bad = result.values.count {|x| !x[0]}

	puts "[+] Notary hits: #{good}\n[+] Notary misses: #{bad}" if $debug
 
	if $config['notary-agreement'] == 'consensus'
		return false if bad
	elsif $config['notary-agreement'] == 'majority'
		return true if good > bad
	# TODO add error message here
	else
		return false
	end 
end

def cache_check(host, port, fingerprint)
	return (!_cache_check(host, port, fingerprint, $db).zero? or
			!_cache_check(host, port, fingerprint, $userdb).zero?)
end

def _cache_check(host, port, fingerprint, db)
	return db.get_first_value('SELECT COUNT(*) FROM fingerprints WHERE location = ? AND fingerprint = ? ' + 
							  ' AND timestamp > ?', "#{host}:#{port}", fingerprint, "#{(Time.now - 60*60*25*7).to_i}") 
end

def cache_update(result, host, port)
	_cache_update(result, host, port, $userdb)

	# If we can update the global cache, do it
	if File.writable?($config['db'])
		_cache_update(result, host, port, $db)
	end
end

def _cache_update(result, host, port, db)
	for r in result.values
		if r[0]
			data = r[1]['fingerprintList']

			data.each do |d|
				print = d['fingerprint']
				db.execute('INSERT OR IGNORE INTO fingerprints (location, fingerprint) ' +
							'VALUES (?, ?)', "#{host}:#{port}", print)
				db.execute('UPDATE fingerprints SET timestamp = ? WHERE location = ? ' + 
						   'AND fingerprint = ?', "#{Time.now.to_i}", "#{host}:#{port}", print)
			end
		end
	end
end

def remote_check(remote_host, remote_port, fingerprint)
	requests = []
	results = {}

	# Loop over all notaries
	$notaries.each do |notary|
		hosts = notary['hosts']
		# Loop over all hosts in a notary
		hosts.each do |host|
			# Build the notary request url
			url = 'https://' + host['host'] + ':' + host['ssl_port'].to_s + 
				'/target/' + remote_host +'+'+ remote_port.to_s

			# Get the public key out of the cert
			cert = OpenSSL::X509::Certificate.new(host['certificate'])

			# Note we don't check SSL validity here, but we do check sigs on response
			r = Typhoeus::Request.new( url,
				:body           => 'fingerprint=' + fingerprint,
				:method         => :post,
				:ssl_verifypeer => false,
				:headers        => {'content-type' => 'application/x-www-form-urlencoded'},
			)

			r.on_complete do |response|
				if response.success?
					# Rip out the signature
					data = JSON.parse(response.body)
					sig = Base64.decode64(data['signature'])
					data.delete('signature')
					# We need to add some spaces to our message because python
					# and ruby use different json encodings which matters for
					# signatures
					message = JSON.generate(data).gsub(/":/, '": ').gsub(/,/, ', ')

					# Check the sig. This makes up for not check the cert on connect
					pub_key = OpenSSL::PKey::RSA.new(cert.public_key)

					if pub_key.verify(OpenSSL::Digest::SHA1.new, sig, message)
						markit_good(results, url, data)
					else
						markit_zero(results, url)
					end
	
				else
					# We dont care why it didnt work, make it false
					markit_zero(results, url)
				end
			end

			# Add to our requests
			requests << r

		end
	end

	hydra = Typhoeus::Hydra.new
	requests.each {|r| hydra.queue r}
	hydra.run

	return results
end

def markit_good(results, url, response)
	results[url] = [true, response]
end

def markit_zero(results, url)
	results[url] = [false]
end

main
