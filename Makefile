CC = gcc
LL = -Wl,-z,relro,-z,now -ldl -lcrypto -lssl -lsqlite3 -ljson -lcurl -lcrypto -lssl -lgnutls
INC = -I./gnutls

CFLAGS = -g -Wall -fPIC -fPIE -pie -D_FORTIFY_SOURCE=2 -Wformat -Wformat-security -fstack-protector-all

all: lib install

lib: sslshim.o converge.o common.o cache.o
	$(CC) -g -shared -fPIC -Wl,-soname -Wl,converge.so -Wl,--no-undefined -o converge.so converge.o sslshim.o common.o cache.o $(LL)

sslshim.o:
	$(CC) $(INC) sslshim.c $(CFLAGS) -c -o sslshim.o

converge.o:
	$(CC) $(INC) converge.c $(CFLAGS) -c -o converge.o

common.o:
	$(CC) common.c $(CFLAGS) -c -o common.o

cache.o:
	$(CC) cache.c $(CFLAGS) -c -o cache.o

b64.o:
	cd cconverge && make b64.o

cconverge.bin:
	cd cconverge && make 

clean:
	rm -f *.o converge.so
	cd cconverge && make clean

install: cconverge.bin
	cp cconverge/cconverge ~/bin/

uninstall:
	rm ${HOME}/bin/cconverge
	rm -rf /var/lib/converge
	rm -rf ${HOME}/.converge
	rm -rf /usr/local/etc/converge
