#define _GNU_SOURCE // RTLD_NEXT

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <netinet/in.h>

#include <dlfcn.h> // dlsym
#include <unistd.h> // getpid/getpgrp
#include <arpa/inet.h> // inet_ntop

#include <sys/types.h> // getaddrinfo, gethostbyname
#include <sys/socket.h> // getaddrinfo, gethostbyname
#include <netdb.h> // getaddrinfo, gethostbyname

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#include "sslshim.h"
#include "converge.h"
#include "common.h"

// XXX GnuTLS does not check cert validity, even with this enabled.
// This option is for checking things like cert expiration, etc. Any value
// returned by this other than invalid CA and cert ok are returned. In the
// case of those two, we notarize the cert. Probably not a bad idea, depends
// on what you're worried about.
#define SSLCHECK 0

// Hook _init and _fini are in converge.c

int getaddrinfo(const char *nodename, const char *servname,
		        const struct addrinfo *hints, struct addrinfo **res) {
	typeof(getaddrinfo) *real_getaddrinfo;
	pid_t pid, grp;
	char addr[INET6_ADDRSTRLEN];
   	const char *ptr = NULL;
	int ret;
	struct addrinfo *p;
	struct sockaddr_in *tmp;
	struct sockaddr_in6 *tmp6;

	real_getaddrinfo = dlsym(RTLD_NEXT, "getaddrinfo");

	ret = (*real_getaddrinfo)(nodename, servname, hints, res);
	
	pid = getpid();
	grp = getpgrp();


	debug_log("\n");
	debug_log("[+] Hooked getaddrinfo() <%d, %d>\n", pid, grp);
	debug_log("\tname: %s\n", nodename);

	for (p = *res; p; p = p->ai_next) {
		switch (p->ai_family) {
			case AF_INET:
				tmp = (struct sockaddr_in *)p->ai_addr;
				ptr = inet_ntop(AF_INET, &tmp->sin_addr, addr, INET_ADDRSTRLEN);
				break;
			case AF_INET6:
				tmp6 = (struct sockaddr_in6 *)p->ai_addr;
				ptr = inet_ntop(AF_INET6, &tmp6->sin6_addr, addr, INET6_ADDRSTRLEN);
				break;
			default:
				ptr = NULL;
				debug_log("[-] Unknown address type <%d> in hooked getaddrinfo(), skipping\n", p->ai_family);
				//exit(221);
		}

		if (ptr) {
			debug_log("\tip: %s\n", addr);

			// Send it to the db
			converge_getaddrinfo(nodename, addr);
		}
	}


	return ret;
}


struct hostent *gethostbyname(const char *name) {
	typeof(gethostbyname) *real_gethostbyname;
	struct hostent *he;
	struct in_addr **addr_list;
	char addr[INET6_ADDRSTRLEN];
	const char *ptr = NULL;
	pid_t pid, grp;
	int i;

	real_gethostbyname = dlsym(RTLD_NEXT, "gethostbyname");

	he = (*real_gethostbyname)(name);
	
	pid = getpid();
	grp = getpgrp();

	debug_log("\n");
	debug_log("[+] Hooked gethostbyname() <%d, %d>\n", pid, grp);
	debug_log("\tname: %s\n", name);

	if (!he) {
		debug_log("\tip: NULL\n");
		return he;
	}

	addr_list = (struct in_addr **)he->h_addr_list;

	for (i = 0; addr_list[i]; i++) {
		switch (he->h_addrtype) {
			case AF_INET:
				ptr = inet_ntop(AF_INET, addr_list[i], addr, INET_ADDRSTRLEN);
				break;
			case AF_INET6:
				ptr = inet_ntop(AF_INET6, addr_list[i], addr, INET6_ADDRSTRLEN);
				break;
			default:
				ptr = NULL;
				debug_log("[-] Unknown address type <%d> in hooked gethostbyname(), skipping\n", he->h_addrtype);
				//exit(222);
		}
		
		if (ptr) {
			debug_log("\tip: %s\n", addr);

			// Send it to the db
			converge_getaddrinfo(name, addr);
		}
	}
	
	return he;
}

int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen) {
	typeof(connect) *real_connect;
	unsigned short port;
	pid_t pid, grp;
	struct sockaddr_in *p;
	struct sockaddr_in6 *p6;
	char address[INET6_ADDRSTRLEN];
	const char *ptr = NULL;
	int skip = 0;

	real_connect = dlsym(RTLD_NEXT, "connect");


	pid = getpid();
	grp = getpgrp();

	debug_log("\n");
	debug_log("[+] Hooked connect() <%d, %d>\n", pid, grp);
	debug_log("\tfd: %d\n", sockfd);
	switch (addr->sa_family) {
		case AF_INET:
			p = (struct sockaddr_in *)addr;
			ptr = inet_ntop(AF_INET, &p->sin_addr, address, INET_ADDRSTRLEN);
			port = ntohs(p->sin_port);
			break;
		case AF_INET6:
			p6 = (struct sockaddr_in6 *)addr;
			ptr = inet_ntop(AF_INET6, &p6->sin6_addr, address, INET6_ADDRSTRLEN);
			port = ntohs(p6->sin6_port);
			break;
		default: // AF_UNIX, others
			debug_log("[-] Unknown address type <%d> in hooked connect(), skipping\n", addr->sa_family);
			skip = 1;
			//exit(223);
	}

	if (!skip) {
		if (ptr)
			debug_log("\tip: %s\n", address);

		debug_log("\tport: %u\n", port);

		converge_connect(address, port, sockfd);
	}

	return (*real_connect)(sockfd, addr, addrlen);
}

// OpenSSL

// XXX forks n shit
/*
 * Ok, so how we deal with forks is going to be kinda tricky
 *
 * So, we want all fork'd processes to share fd, which would 
 * break the (pid, fd) mapping. Instead we're going to use a
 * (pid, parent pid, fd) for a key. Essentally, if we look for
 * a fd:
 *  if getpid() == getpgrp()
 *    select pid, fd
 *  else
 *    select parent_pid, fd 
 *      or
 *    select pid, fd
 *
 * The latter case works like this: if we're a child process
 * we can read our parents fd's legally. However, we can also
 * create our own fd's which are legit, but we don't want other
 * child proccess, or the parent process to consider them legit
 *
 * TODO what happens to a child process when the parent process
 * dies. Other corner cases for process termination and creation
 */
 
 // Hook the SSL handshake function and then pipe the verification through Convergence at the end.
int SSL_do_handshake(SSL *ssl){
	typeof(SSL_do_handshake) *real_SSL_do_handshake;
	char *host = NULL, *port = NULL, fingerprint[EVP_MAX_MD_SIZE + EVP_MAX_MD_SIZE/2];
	int realret, convret, vmode;
	unsigned int fprint_size;
	X509 *cert = NULL;
	const EVP_MD *fprint_type;
	unsigned char fprint[EVP_MAX_MD_SIZE];
	pid_t pid, grp;

	real_SSL_do_handshake = dlsym(RTLD_NEXT, "SSL_do_handshake");

	// We will take care of the verification ourself
	vmode = ssl->verify_mode;
	ssl->verify_mode=SSL_VERIFY_NONE;

	realret = (*real_SSL_do_handshake)(ssl);
	
	ssl->verify_mode = vmode;

    pid = getpid();
    grp = getpgrp();

    debug_log("\n");
    debug_log("[+] Hooked SSL_do_handshake() <%d,%d>\n", pid, grp);

	converge_get_host_openssl(ssl, &host, &port);

	cert = SSL_get_peer_certificate(ssl);

	if (!cert) {
		debug_log("[-]No certificate presented in SSL_do_handshake\n");
		exit(223);
	}

	fprint_type = EVP_sha1();

	if (!X509_digest(cert, fprint_type, fprint, &fprint_size)) {
		debug_log("[-] Bad fingerprint in SSL_do_handshake\n");
		exit(223);
	}

	_sha_to_ascii(fprint, fingerprint, fprint_size);

	convret = converge(host, port, fingerprint);

	free(host);
	free(port);

	//TODO We should probably do SSL_get_error() on realret and check to see if it failed on its own.
	if (!convret) {
		debug_log("[+] converge returned: %d, do_handshake returned %d, returning %d.\n", convret, realret, realret);
		ssl->verify_result = X509_V_OK;
		ssl->session->verify_result = ssl->verify_result;
		return realret;
	} else {
		debug_log("[+] converge returned: %d, do_handshake returned %d, abort connection.\n", convret, realret);
		ssl->verify_result = X509_V_ERR_INVALID_CA;
		ssl->session->verify_result = ssl->verify_result;
		SSLerr(SSL_F_SSL3_GET_SERVER_CERTIFICATE,SSL_R_CERTIFICATE_VERIFY_FAILED);

		return -1;
	}

} 


long SSL_get_verify_result(const SSL *ssl) {
	char *host = NULL, *port = NULL, fingerprint[EVP_MAX_MD_SIZE + EVP_MAX_MD_SIZE/2];
	int ret;
	unsigned int fprint_size;
	X509 *cert = NULL;
	const EVP_MD *fprint_type;
	unsigned char fprint[EVP_MAX_MD_SIZE];
	pid_t pid, grp;

	/*
	 * Convergence just makes sure the certificates are the same, it does not
	 * check for expiration, revocation, etc. There is some overlap here we
	 * don't want (revocation specifically), but it's a good catchall that
	 * prevents us from having to check all the other error conditions ourselfs
	 */
#if SSLCHECK
	typeof(SSL_get_verify_result) *real_SSL_get_verify_result;

	real_SSL_get_verify_result = dlsym(RTLD_NEXT, "SSL_get_verify_result");

	ret = (*real_SSL_get_verify_result)(ssl);

	if (ret != X509_V_OK && ret != X509_V_ERR_INVALID_CA)
		return ret;
#endif

	pid = getpid();
	grp = getpgrp();

	debug_log("\n");
	debug_log("[+] Hooked SSL_get_verify_result() <%d, %d>\n", pid, grp);

	converge_get_host_openssl(ssl, &host, &port);

	cert = SSL_get_peer_certificate(ssl);

	//printf("Certificate:\n");
	//PEM_write_X509(stdout, cert);

	if (!cert) {
		debug_log("[-]No certificate presented in SSL_get_verify_result\n");
		exit(223);
	}

	fprint_type = EVP_sha1();

	if (!X509_digest(cert, fprint_type, fprint, &fprint_size)) {
		debug_log("[-] Bad fingerprint in SSL_get_verify_result\n");
		exit(223);
	}

	_sha_to_ascii(fprint, fingerprint, fprint_size);

	ret = converge(host, port, fingerprint);

	free(host);
	free(port);

	// TODO We should check the return code from convergence and
	// return something more accurate
	if (!ret) {
		debug_log("[+] converge returned: %d, returning X509_V_OK\n", ret);
		return X509_V_OK;
	} else {
		debug_log("[+] converge returned: %d, returning X509_V_ERR_INVALID_CA\n",
				ret);
		return X509_V_ERR_INVALID_CA;
	}

}

// GnuTLS

// Not sure if this is present in our current lib version, and is untested
int gnutls_certificate_verify_peers3(gnutls_session_t session, char *host, unsigned int *status) {
	return gnutls_certificate_verify_peers2(session, status);
}

int gnutls_certificate_verify_peers2(gnutls_session_t session, unsigned int *status) {
	pid_t pid, grp;
	cert_auth_info_t info;
	gnutls_x509_crt_t peer_cert;
	int ret;
	char *host, *port, fingerprint[MAX_HASH_SIZE*3+1];
	unsigned char fprint[MAX_HASH_SIZE];
	unsigned int fprint_size = MAX_HASH_SIZE;

	pid = getpid();
	grp = getpgrp();

	debug_log("\n");
	debug_log("[+] Hooked gnutls_certificate_verify_peers2() <%d, %d>\n", pid, grp);
	

	// Hostname is located here if we want it
	// ((server_name_ext_st *)(session->internals.extension_int_data[0].priv.ptr)).server_names.name
	//
	// No port info though as far as I can tell...
	
	converge_get_host_gnutls(session, &host, &port);
	
	info = session->key->auth_info;

	if (!info) {
		*status = GNUTLS_E_NO_CERTIFICATE_FOUND;
		return -1;
	}

	ret = gnutls_x509_crt_init(&peer_cert);
	
	if (ret < 0) {
		debug_log("[-] gnutls_certificate_verify_peers2(): cannot get cert\n");
		exit(224);
	}

	ret = gnutls_x509_crt_import(peer_cert, &info->raw_certificate_list[0], GNUTLS_X509_FMT_DER);

	if (ret < 0) {
		debug_log("[-] gnutls_certificate_verify_peers2(): cannot get cert\n");
		exit(224);
	}
	
	ret = gnutls_x509_crt_get_fingerprint(peer_cert, GNUTLS_DIG_SHA1, fprint, &fprint_size);

	if (ret < 0) {
		debug_log("[-] gnutls_certificate_verify_peers2(): cannot get SHA1 sum of cert\n");
		exit(224);
	}

	_sha_to_ascii(fprint, fingerprint, fprint_size);

	ret = converge(host, port, fingerprint);

	free(host);
	free(port);

	if (ret) {
		debug_log("[+] converge returned: %d, returning 0 ad not setting status\n", ret);
		ret = 0;
	} else {
		debug_log("[+] converge returned: %d, returning -1 and setting status\n",
				ret);
		*status |= GNUTLS_E_CERTIFICATE_ERROR;
		ret = -1;
	}

	return ret;
}

void _sha_to_ascii(const unsigned char *in, char *out, unsigned int size) {
	int i;
	for (i = 0; i < size - 1; i++) {
		snprintf(&out[i*3], 4,"%02X:", in[i]);
	}
	snprintf(&out[i*3], 4, "%02X", in[i]);
}
