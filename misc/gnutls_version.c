#include <stdio.h>
#include <gnutls/gnutls.h>

int main()
{
	const char * version = gnutls_check_version(NULL);
	printf("Version: %s \n", version);
	return 0;
}
