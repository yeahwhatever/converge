#include <string.h>
#include <stdlib.h>

#include <curl/curl.h>
#include <json/json.h>

#include <sys/types.h> // fstat
#include <sys/stat.h>
#include <unistd.h>

#include <fcntl.h> // open

#include <dirent.h> // readdir

#include <openssl/bio.h> // BIO_new_mem_buf
#include <openssl/evp.h> // EVP_MD_CTX_init
#include <openssl/pem.h> // EVP_MD_CTX_init
#include <openssl/err.h> // ERR_get_error
#include <openssl/x509.h> // ERR_get_error

#include "converge_client.h"
#include "cdecode.h"

#define CONFIG_PATH "/usr/local/etc/converge/converge.config"

#define FULLRESP 1

converge_config config;
notaries *notes = NULL;

int converge_client_do_verify(const char *host, const char *port, const char *print) {
	notaries *n;
	int j, i = 0, num_hosts = 0, ret = 0, good = 0, bad = 0;
	CURL **http;
	CURLM *mhandle;
	CURLcode cret;
	char post[100], url[500], ssl_port[10];
	notary_msg *note_resp;
	long *note_code;

	sprintf(post, "fingerprint=%s", print);
	mhandle = curl_multi_init();

	// Total up the number of hosts we have
	for (n = notes; n; n = n->n_next)
		num_hosts += n->num_hosts;

	http = xmalloc(num_hosts * sizeof(CURL *));
	note_resp = xmalloc(num_hosts * sizeof(notary_msg));
	note_code = xmalloc(num_hosts * sizeof(long));

	for (n = notes; n; n = n->n_next) {
		// Foreach host in the notary
		for (j = 0; j < n->num_hosts; j++) {
			http[i] = curl_easy_init();

			memset((void *)&note_resp[i], 0, sizeof(notary_msg));
			note_resp[i].num_note = j;
			note_resp[i].note = n;

			sprintf(ssl_port, "%d", n->n[j].ssl_port);
			ssl_port[9] = '\0';

			sprintf(url, "https://%s:%s/target/%s+%s",
					n->n[j].host, ssl_port, host, port);
			url[499] = '\0';

			cret = curl_easy_setopt(http[i], CURLOPT_URL, url);
			cret = curl_easy_setopt(http[i], CURLOPT_POSTFIELDS, post);
			cret = curl_easy_setopt(http[i], CURLOPT_WRITEDATA,
					(void *)&note_resp[i]);
			cret = curl_easy_setopt(http[i], CURLOPT_WRITEFUNCTION, _notary_cb);
			// Disable ssl verification because we'll check the sig...
			cret = curl_easy_setopt(http[i], CURLOPT_SSL_VERIFYPEER, 0);

			cret = curl_multi_add_handle(mhandle, http[i]);
			
			// Inc our counter
			i++;
		}
	}

	// Straight from http://curl.haxx.se/libcurl/c/multi-single.html
	int still_running = 0;

	cret = curl_multi_perform(mhandle, &still_running);

	do {
		struct timeval timeout;
		int rc; /* select() return code */ 

		fd_set fdread;
		fd_set fdwrite;
		fd_set fdexcep;
		int maxfd = -1;

		long curl_timeo = -1;

		FD_ZERO(&fdread);
		FD_ZERO(&fdwrite);
		FD_ZERO(&fdexcep);

		/* set a suitable timeout to play around with */ 
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;

		cret = curl_multi_timeout(mhandle, &curl_timeo);

		if(curl_timeo >= 0) {
			timeout.tv_sec = curl_timeo / 1000;
			if(timeout.tv_sec > 1)
				timeout.tv_sec = 1;
			else
				timeout.tv_usec = (curl_timeo % 1000) * 1000;
		}

		/* get file descriptors from the transfers */ 
		cret = curl_multi_fdset(mhandle, &fdread, &fdwrite, &fdexcep, &maxfd);

		rc = select(maxfd+1, &fdread, &fdwrite, &fdexcep, &timeout);

		switch (rc) {
			case -1:
				/* select error */ 
				still_running = 0;
				break;
			case 0:
			default:
				/* timeout or readable/writable sockets */ 
				cret = curl_multi_perform(mhandle, &still_running);
				break;
		}
	} while (still_running);

	curl_multi_cleanup(mhandle);

	// Get the response code for each notary and clean up
	for (i = 0; i < num_hosts; i++) {
		curl_easy_getinfo(http[i], CURLINFO_RESPONSE_CODE, &note_code[i]);
		curl_easy_cleanup(http[i]);
	}

	free(http);

	// Go through responses and see if they're valid (http code and sig)
	for (i = 0; i < num_hosts; i++) {
		
		debug_log("[*] converge_client_do_vertify: \n"
				"\tHost: %s\tStatus:%d\n", 
				note_resp[i].note->n[note_resp[i].num_note].host, 
				note_code[i]);
#if FULLRESP
		debug_log("\tResp: %s\n", note_resp[i].resp);
#endif
		if (note_code[i] != 200) 
			bad++;
		else {
			n = note_resp[i].note;
			j = note_resp[i].num_note;
			if (_resp_valid(note_resp[i].resp, n->n[j].cert)) {
				good++;
				_update_cache(host, port, print);
			} else
				bad++;
		}
	}

	// Check agreement type
	ret = -1;

	debug_log("[*] converge_client_do_verify:\n\tgood: %d\n\tbad: %d\n", good, bad);

	if (!strcmp(config.notary_agreement, "majority") && good > bad) 
		ret = 0;
	else if (!strcmp(config.notary_agreement, "consensus") && !bad) 
		ret = 0;

	// Clean up
	free(note_resp);
	free(note_code);

	return ret;
}

size_t _notary_cb(void *buffer, size_t size, size_t nmemb, void *userp) {
	int segsize = size * nmemb;
	notary_msg *nm;

	nm = (notary_msg *)userp;

	if (nm->resp_index + segsize > RESP_SIZE) {
		// Maybe need to pass an error back?
		return 0;
	}

	strncpy(&nm->resp[nm->resp_index], buffer, RESP_SIZE);

	nm->resp_index += segsize;

	// This should work to null terminate, I think.
	nm->resp[nm->resp_index] = '\0';

	// Tell CURL things are cool
	return segsize;
}

int _resp_valid(const char *resp, const char *cert) {
	int ret;

	BIO *b;
	X509 *x;
	EVP_PKEY *ep;
	EVP_MD_CTX c;

	base64_decodestate ds;
	json_object *obj, *thing;

	char buf[120];
	unsigned char sig[700], sig_64[400], *msg;
	unsigned int sig_len;

	// General Init
	OpenSSL_add_all_digests();
	base64_init_decodestate(&ds);

	// Load the response
    obj = json_tokener_parse(resp);

	// Cant parse resp
	if (!obj) {
		ret = 0;
		goto exit1;
	}

	// Rip the sig out
    thing = json_object_object_get(obj, "signature");
    strncpy(sig_64, json_object_get_string(thing), 400);
    sig_64[399] = '\0';
    json_object_put(thing);

	// Kill the sig
    json_object_object_del(obj, "signature");

	// Get the json in our memory, and make it like python's json.dumps()
	msg = xmalloc(strlen(json_object_get_string(obj)));

    strcpy(msg, json_object_get_string(obj));

	// Kill the json object
	json_object_put(obj);

    _make_like_python(msg);

	// Decode base64
	sig_len = base64_decode_block(sig_64, strlen(sig_64), sig, &ds);

	// Set up the sig verification
	b = BIO_new_mem_buf((void *)cert, strlen(cert));

	x = PEM_read_bio_X509(b, (X509 **)NULL, 0, (void *)NULL);

	if (!x) {
		printf("Couldn't read x509 cert\n");
		ret = 0;
		goto exit2;
	}

	ep = X509_get_pubkey(x);

	if (!ep) {
		printf("Couldn't read pubkey cert\n");
		ret = 0;
		goto exit2;
	}


	EVP_MD_CTX_init(&c);
	EVP_VerifyInit(&c, EVP_get_digestbyname("SHA1"));
	EVP_VerifyUpdate(&c, msg, strlen(msg));
	ret = EVP_VerifyFinal(&c, sig, sig_len, ep);

	if (ret == -1) {
		printf("%s\n", ERR_error_string(ERR_get_error(), buf));
		ret = 0;
	}
	
	exit2:
	free(msg);
	exit1:
	EVP_cleanup();

	return ret;
}

void _update_cache(const char *host, const char *port, const char *print) {

}

int _check_cache(const char *host, const char *port, const char *print) {
	return 0;
}

void converge_client_init() {
	int ret;
	DIR *d;
	struct dirent *de;
	notaries *n;
	char *buf;

	_load_config();

	// Get our specific notaries
	d = opendir(config.notaries_dir);

	if (!d) {
		perror("converge_client_init - opendir");
		exit(211);
	}

	n = notes;
	for (de = readdir(d); de; de = readdir(d)) {
		if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, ".."))
			continue;

		buf = xmalloc(strlen(de->d_name) + strlen(config.notaries_dir) + 1);
		sprintf(buf, "%s%s", config.notaries_dir, de->d_name);

		n = _load_notary(buf, n);
		free(buf);
	}

	ret = closedir(d);

	if (ret == -1) {
		perror("converge_client_init - closedir");
		exit(211);
	}
}

void converge_client_clean() {
	notaries *p, *killme;

	p = notes;

	while (p) {
		killme = p;
		p = p->n_next;

		free(killme);
	}
}

void _load_config() { 
	char *buf;
	json_object *obj, *thing;

	buf = _safe_load(CONFIG_PATH);

	// Parse the config
	obj = json_tokener_parse(buf);

	// Free the config
	free(buf);

	// Populate our global config object
	thing = json_object_object_get(obj, "db");
	strncpy(config.db, json_object_get_string(thing), TEXT_SIZE);
	config.db[TEXT_SIZE - 1] = '\0';
	json_object_put(thing);

	thing = json_object_object_get(obj, "userdb");
	strncpy(config.userdb, json_object_get_string(thing), TEXT_SIZE);
	config.userdb[TEXT_SIZE - 1] = '\0';
	json_object_put(thing);

	thing = json_object_object_get(obj, "notaries-dir");
	strncpy(config.notaries_dir, json_object_get_string(thing), TEXT_SIZE);
	config.notaries_dir[TEXT_SIZE - 1] = '\0';
	json_object_put(thing);

	thing = json_object_object_get(obj, "notary-agreement");
	strncpy(config.notary_agreement, json_object_get_string(thing), TEXT_SIZE);
	config.notary_agreement[TEXT_SIZE - 1] = '\0';
	json_object_put(thing);

	// Free the json object. 
	json_object_put(obj);
}

notaries *_load_notary(const char *s, notaries *n) {
	notaries *tmp;
	notary *host;
	char *buf;
	json_object *obj, *thing, *host_elem, *hash_elem;
	int i, len;

	buf = _safe_load(s);

	obj = json_tokener_parse(buf);

	free(buf);

	tmp = xmalloc(sizeof(notaries));
	tmp->n_next = NULL;

	// Version
	thing = json_object_object_get(obj, "version");
	tmp->version = json_object_get_int(thing);
	json_object_put(thing);

	// Iterate through the hosts and store them
	thing = json_object_object_get(obj, "hosts");
	len = json_object_array_length(thing);

	for (i = 0; i < len && i < MAX_NOTARY; i++) {
		host_elem = json_object_array_get_idx(thing, i);

		hash_elem = json_object_object_get(host_elem, "host");
		strncpy(tmp->n[i].host, json_object_get_string(hash_elem), TEXT_SIZE);
		tmp->n[i].host[TEXT_SIZE - 1] = '\0';
		json_object_put(hash_elem);

		hash_elem = json_object_object_get(host_elem, "http_port");
		tmp->n[i].http_port = json_object_get_int(hash_elem);
		json_object_put(hash_elem);

		hash_elem = json_object_object_get(host_elem, "ssl_port");
		tmp->n[i].ssl_port = json_object_get_int(hash_elem);
		json_object_put(hash_elem);

		hash_elem = json_object_object_get(host_elem, "certificate");
		strncpy(tmp->n[i].cert, json_object_get_string(hash_elem), CERT_SIZE);
		tmp->n[i].cert[CERT_SIZE - 1] = '\0';
		json_object_put(hash_elem);

		json_object_put(host_elem);
	}
	json_object_put(thing);

	// Store the number of hosts this notary has
	tmp->num_hosts = i;

	// Name
	thing = json_object_object_get(obj, "name");
	strncpy(tmp->name, json_object_get_string(thing), TEXT_SIZE);
	tmp->name[TEXT_SIZE - 1] = '\0';
	json_object_put(thing);

	// Bundle location
	thing = json_object_object_get(obj, "bundle_location");
	strncpy(tmp->bundle, json_object_get_string(thing), TEXT_SIZE);
	tmp->bundle[TEXT_SIZE - 1] = '\0';
	json_object_put(thing);

	// Free the parent object
	json_object_put(obj);

	// If there are no notaries yet
	if (!n) {
		notes = tmp;
	} else {
		n->n_next = tmp;
	}

	return tmp;
}


// You are responsible for freeing memory from this function
// Don't fuck up.
char *_safe_load(const char *s) {
	struct stat buf;
	int ret, fd, total;
	char *tmp;

	// Open the config
	fd = open(s, O_RDONLY);

	if (fd == -1) {
		perror("_safe_load - open");
		exit(212);
	}
	
	// Get the size
	ret = fstat(fd, &buf);

	if (ret == -1) {
		perror("_safe_load - fstat");
		exit(212);
	}

	// Alloc a buffer that size
	tmp = xmalloc(buf.st_size);

	// Load it
	total = 0;

	while (total < buf.st_size) {
		ret = read(fd, tmp, buf.st_size);
		
		if (ret == -1) {
			perror("_safe_load - read");
			exit(212);
		}

		total += ret;
	}

	// clean up fd
	ret = close(fd);

	if (ret == -1) {
		perror("_safe_load - close");
		exit(212);
	}

	return tmp;
}

void _make_like_python(char *s) {
    int len = 0, i, j;

    len = strlen(s);

    // Fucking python json.dumps()
    for (i = 0; i < len; i++) 
        if (s[i] == ' ') 
            if (!i || (s[i-1] != ':' && s[i-1] != ',')) {
                for (j = i; j < len - 1; j++) 
                    s[j] = s[j+1];
                s[j] = '\0';
            }   

}

