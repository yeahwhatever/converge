#include <sys/types.h>

#include "converge_client.h"

int main(int argc, char **argv) {
    int ret = 0;
    
	if (argc != 4)
		return 0;

    char *host = argv[1]; //"gmail.com";
    char *port = argv[2]; //"443";
	char *print = argv[3];

	// Good
    //char *print = "43:83:A3:31:32:2C:CB:0A:44:9A:4B:E8:77:9A:3D:E8:E9:68:B7:FB";

    converge_client_init();

    ret = converge_client_do_verify(host, port, print);

    converge_client_clean();

    return ret;
}
