#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <openssl/ssl.h>

#include <unistd.h> // getpid/getpgrp

#include <sys/wait.h> // waitpid

#include <sqlite3.h> // sqlite shit

#include "converge.h"
#include "common.h"
#include "cache.h"

#define SHIM_NAME       "converge.so"
#define HOOK_DB         "/.converge/hook.db"
#define CACHE_DB        "/.converge/cache.db"
#define SYS_CACHE_DB    "/var/lib/converge/cache.db"

#define PRINT_INIT 0

extern char **environ;
extern FILE *logfd;

// Global db pointers
sqlite3 *db;
sqlite3 *cache_db;
sqlite3 *sys_cache_db;

void converge_init() {
	int ret;
	pid_t pid, grp;
	char *home, *dbloc;

	pid = getpid();
	grp = getpgrp();

#if PRINT_INIT
	debug_log("\n");
	debug_log("[+] Hooked init <%d, %d>\n", pid, grp);
	debug_log("\tclearing db\n");
#endif

	home = getenv("HOME");
	
    // Setup hook db
	dbloc = xmalloc(strlen(home) + strlen(HOOK_DB) + 1);

	sprintf(dbloc, "%s%s", home, HOOK_DB);

	ret = sqlite3_open_v2(dbloc, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, NULL);

	if (ret != SQLITE_OK) {
		debug_log("[-] Could not open db (%s): %s\n", dbloc, sqlite3_errmsg(db));
		exit(241);
	}

	free(dbloc);

	converge_db_clear(pid, grp);

    // Setup caching DB
	dbloc = xmalloc(strlen(home) + strlen(CACHE_DB) + 1);

	sprintf(dbloc, "%s%s", home, CACHE_DB);

	ret = sqlite3_open_v2(dbloc, &cache_db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, NULL);

	free(dbloc);

	if (ret != SQLITE_OK) {
		debug_log("[-] Could not open cache db: %s\n", sqlite3_errmsg(cache_db));
		exit(241);
	}

	ret = sqlite3_open_v2(SYS_CACHE_DB, &sys_cache_db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, NULL);

	if (ret != SQLITE_OK) {
		debug_log("[-] Could not open system cache db: %s\n", 
                sqlite3_errmsg(sys_cache_db));
		exit(241);
	}

}

void converge_fini() {
	// See todo below
	//pid_t pid, grp;
	int ret;

	//pid = getpid();
	//grp = getpgrp();

#if PRINT_INIT	
	debug_log("\n");
	debug_log("[+] Hooked fini <%d, %d>\n", pid, grp);
	debug_log("\tclearing db\n");
#endif

	// XXX TODO 
	// We need to change this. It needs to be smarter in how it clears the db
	// when an app exits, its children pgrp gets set to 1, so if we clear all
	// child apps will not be able to access the parents fd
	//converge_db_clear(pid, grp);

    // close hook db
	ret = sqlite3_close(db);

	while (ret == SQLITE_BUSY)
		ret = sqlite3_close(db);

	if (ret != SQLITE_OK) {
		debug_log("[-] Could not close db: %s\n", sqlite3_errmsg(db));
		exit(242);
	}

    // close cache db
	ret = sqlite3_close(cache_db);

	while (ret == SQLITE_BUSY)
		ret = sqlite3_close(cache_db);

	if (ret != SQLITE_OK) {
		debug_log("[-] Could not close cache db: %s\n", sqlite3_errmsg(cache_db));
		exit(242);
	}

    // close system wide cache db
	ret = sqlite3_close(sys_cache_db);

	while (ret == SQLITE_BUSY)
		ret = sqlite3_close(sys_cache_db);

	if (ret != SQLITE_OK) {
		debug_log("[-] Could not close system cache db: %s\n", 
                sqlite3_errmsg(cache_db));
		exit(242);
	}

	// XXX TODO we need to close the log file, and deal with the whole
	// many processes writing to the log file. Pain in the ass, but not
	// critical. For "production" use, it will just write to /dev/null
	// anyways
}

int notarize(char *host, char *port, char *fingerprint) 
{
	char **env;
	int ret = 1, forkret;
	char *args[5];
	pid_t pid;

	debug_log("[+] Calling cconverge with exec(): %s %s %s %s %s\n", 
			"cconverge", "cconverge", host, port, fingerprint);

	env = copy_env();
	remove_shim(env);

	pid = vfork();

	if (pid) {
		// parent
		waitpid(pid, &ret, 0);
	} else {
		// child
		args[0] = "cconverge";
		args[1] = host;
		args[2] = port;
		args[3] = fingerprint;
		args[4] = NULL;

		forkret = execvpe(args[0], args, env);
        // If we reach this code, it means that cconverge failed to execute.
		perror("converge");
		debug_log("[-] execvpe returned: %d\n", forkret);
		exit(243);

	}

	free_env(env);
	
	if (WIFEXITED(ret))
		return WEXITSTATUS(ret);
	else
		// Return -1 for error
		return -1;
}

int converge(char *host, char *port, char *fingerprint) {
    int err, ret;

	// Check our user cache, then check the system cache
    ret = check_cache(host, port, fingerprint, cache_db);
    if (ret == 0) // we had a cache miss from the user's cache
        ret = check_cache(host, port, fingerprint, sys_cache_db);

	if (ret) {
		debug_log("[+] Cache hit for (%s, %s, %s) return OK\n", host, port,
				fingerprint);
		return ret;
	}

    // If we did not have any cache hits, notarize
	ret = notarize(host, port, fingerprint);

	// Update our caches
	if (ret == 1) {
		err = update_cache(host, port, fingerprint, cache_db);
		if (err == -1 ) {
			debug_log("[-] Could not write to user cache.\n");
			exit(244);
		}
		// don't bother checking the return from system cache
		update_cache(host, port, fingerprint, sys_cache_db);
	}

	return ret;
}

void converge_getaddrinfo(const char *host, const char *ip) {
	pid_t pid, grp;
	// sqlite> create table getaddrinfo (pid int, grp int, host text, ip text);
	char *q = "INSERT INTO getaddrinfo VALUES (?, ?, ?, ?);";
	sqlite3_stmt *stmt;
	int ret;

	pid = getpid();
	grp = getpgrp();

	ret = sqlite3_prepare_v2(db, q, -1, &stmt, NULL);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_getaddrinfo prepare: %s\n", sqlite3_errmsg(db));
		exit(245);
	}

	// Bind pid
	ret = sqlite3_bind_int(stmt, 1, pid);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_getaddrinfo bind pid: %s\n", sqlite3_errmsg(db));
		exit(245);
	}

	// Bind grp
	ret = sqlite3_bind_int(stmt, 2, grp);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_getaddrinfo bind grp: %s\n", sqlite3_errmsg(db));
		exit(245);
	}

	// Bind hostname
	ret = sqlite3_bind_text(stmt, 3, host, -1, SQLITE_STATIC);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_getaddrinfo bind host: %s\n", sqlite3_errmsg(db));
		exit(245);
	}

	// Bind ip
	ret = sqlite3_bind_text(stmt, 4, ip, -1, SQLITE_STATIC);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_getaddrinfo bind: %s\n", sqlite3_errmsg(db));
		exit(245);
	}

	ret = sqlite3_step(stmt);

	if (ret != SQLITE_DONE) {
		debug_log("[-] converge_getaddrinfo step: %s\n", sqlite3_errmsg(db));
		exit(245);
	}

	ret = sqlite3_finalize(stmt);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_getaddrinfo finalize: %s\n", sqlite3_errmsg(db));
		exit(245);
	}

}

void converge_connect(char *ip, unsigned int port, int fd) {
	pid_t pid, grp;
	// sqlite> create table connect (pid int, grp int, ip text, port int, fd int, 
	//   constraint connect_unique unique(pid, grp, fd) on conflict replace);
	char *q = "INSERT INTO connect VALUES (?, ?, ?, ?, ?);";
	sqlite3_stmt *stmt;
	int ret;

	pid = getpid();
	grp = getpgrp();
	
	ret = sqlite3_prepare_v2(db, q, -1, &stmt, NULL);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_connect prepare: %s\n", sqlite3_errmsg(db));
		exit(246);
	}

	// Bind pid
	ret = sqlite3_bind_int(stmt, 1, pid);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_connect bind pid: %s\n", sqlite3_errmsg(db));
		exit(246);
	}

	// Bind grp
	ret = sqlite3_bind_int(stmt, 2, grp);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_connect bind grp: %s\n", sqlite3_errmsg(db));
		exit(246);
	}

	// Bind ip 
	ret = sqlite3_bind_text(stmt, 3, ip, -1, SQLITE_STATIC);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_connect bind ip: %s\n", sqlite3_errmsg(db));
		exit(246);
	}

	// Bind port
	ret = sqlite3_bind_int(stmt, 4, port);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_connect bind port: %s\n", sqlite3_errmsg(db));
		exit(246);
	}

	// Bind fd
	ret = sqlite3_bind_int(stmt, 5, fd);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_connect bind fd: %s\n", sqlite3_errmsg(db));
		exit(246);
	}

	ret = sqlite3_step(stmt);

	if (ret != SQLITE_DONE) {
		debug_log("[-] converge_connect step: %s\n", sqlite3_errmsg(db));
		exit(246);
	}

	ret = sqlite3_finalize(stmt);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_connect finalize: %s\n", sqlite3_errmsg(db));
		exit(246);
	}
}

void converge_get_host_openssl(const SSL *ssl, char **host, char **port) {
	if (!ssl) {
		debug_log("[-] Bad struct SSL* to SSL_get_verify_result()\n");
		exit(247);
	}

	// This is kinda weird, basically we want to run these functions until one
	// returns true, then stop.
	if (converge_bio_host(ssl, host, port))
		;
	else if (converge_fd_host_ssl(ssl, host, port))
		;
	else if (!*host || !*port) {
		debug_log("[-] Could not get host:port\n");
		exit(247);
	}
}

void converge_get_host_gnutls(const gnutls_session_t session, char **host, char **port) {
	if (!session) {
		debug_log("[-] Bad struct gnutls_session_t* to _gnutls_x509_cert_verify_peers()\n");
		exit(248);
	}

	if (converge_fd_host_gnutls(session, host, port))
		;
	else if (!*host || !*port) {
		debug_log("[-] Could not get host:port\n");
		exit(248);
	}
}

int converge_bio_host(const SSL *ssl, char **host, char **port) {
	// First we try to get the hostname/port out of the SSL data struct
	// This will work if they used functions like BIO_set_conn_hostname()
	BIO_CONNECT *data = ssl->rbio->ptr;
	unsigned int len;

	if (!ssl->rbio)
		return 0;

	// XXX We might not want to use strlen here, possible null char in str
	if (data && strlen(data->param_hostname) && strlen(data->param_port)) {
		len = strlen(data->param_hostname) + 1;
		*host = xmalloc(len);
		strncpy(*host, data->param_hostname, len);
		*host[len - 1] = '\0';

		len = strlen(data->param_port) + 1;
		*port = xmalloc(len);
		strncpy(*port, data->param_port, len);
		*port[len - 1] = '\0';

		debug_log("[+] converge_bio_host()\n");
		debug_log("\thost: %s:%s\n", *host, *port);

		return 1;
	}

	return 0;
}

int converge_fd_host_ssl(const SSL *ssl, char **host, char **port) {
	int fd = -1;
	pid_t pid, grp;

	pid = getpid();
	grp = getpgrp();

	if (!ssl->rbio)
		return 0;

	debug_log("[+] converge_fd_host_ssl() <%d, %d>\n", pid, grp);

	// XXX Note, reading the code indicates that ssl->rbio and ssl->wbio should
	// be the same, but they're not. Presumably they have different fd's in
	// their respective num fields. Im fairly certain this is correct, but we
	// should investigate and try to figure out why rbio != wbio
	fd = ssl->rbio->num;

	return converge_get_fd_host(pid, grp, fd, host, port);
}

int converge_fd_host_gnutls(const gnutls_session_t session, char **host, char **port) {
	int fd = -1;
	pid_t pid, grp;

	pid = getpid();
	grp = getpgrp();

	debug_log("[+] converge_fd_host_gnutls() <%d, %d>\n", pid, grp);

	fd = (int)session->internals.transport_send_ptr;

	return converge_get_fd_host(pid, grp, fd, host, port);
}

int converge_get_fd_host(pid_t pid, pid_t grp, int fd, char **host, char **port) {
	if (pid == grp || grp != 1) {
		converge_fd_host_parent(pid, grp, fd, host, port);
	} else if (grp == 1) {
		// child where parent has been killed
	}

	if (*host && strlen(*host) && *port && strlen(*port)) {
		debug_log("[+] converge_get_fd_host()\n");
		debug_log("\thost: %s:%s\n", *host, *port);
		return 1;
	}

	return 0;
}

void converge_fd_host_parent(pid_t pid, pid_t grp, int fd, char **host, char **port) {
	sqlite3_stmt *stmt;
	int ret, columns, col;
	unsigned int len;
	const char *val;
	char *query = "SELECT DISTINCT connect.port, getaddrinfo.host "
			"FROM connect INNER JOIN getaddrinfo ON connect.pid = getaddrinfo.pid "
			"AND connect.ip = getaddrinfo.ip "
			"WHERE connect.fd = ? AND (connect.pid = ? OR connect.grp = ?);";

	debug_log("[+] converge_fd_host_parent() query:\n");
	debug_log("\t SELECT DISTINCT connect.port, getaddrinfo.host "
		"FROM connect INNER JOIN getaddrinfo ON connect.pid = getaddrinfo.pid "
		"AND connect.ip = getaddrinfo.ip "
		"WHERE connect.fd = %d AND (connect.pid = %d OR connect.grp = %d);\n", fd, pid, grp);

	ret = sqlite3_prepare_v2(db, query, -1, &stmt, NULL);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_fd_host_parent prepare: %s\n", sqlite3_errmsg(db));
		exit(248);
	}

	// Bind fd
	ret = sqlite3_bind_int(stmt, 1, fd);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_fd_host_parent bind fd: %s\n", sqlite3_errmsg(db));
		exit(248);
	}

	// Bind pid
	ret = sqlite3_bind_int(stmt, 2, pid);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_fd_host_parent bind pid: %s\n", sqlite3_errmsg(db));
		exit(248);
	}

	ret = sqlite3_bind_int(stmt, 3, grp);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_fd_host_parent bind grp: %s\n", sqlite3_errmsg(db));
		exit(248);
	}

	columns = sqlite3_column_count(stmt);

	while (1) {
		ret = sqlite3_step(stmt);

		if (ret == SQLITE_ROW) {
			for (col = 0; col < columns; col++) {
				val = (const char*)sqlite3_column_text(stmt,col);
				len = strlen(val) + 1;

				if (!strcmp(sqlite3_column_name(stmt, col), "port")) {
					*port = xmalloc(len);
					strncpy(*port, val, len);
					debug_log("[+] converge_fd_host_parent port name: %s\n", *port);
				} else if (!strcmp(sqlite3_column_name(stmt, col), "host")) {
					*host = xmalloc(len);
					strncpy(*host, val, len);
					debug_log("[+] converge_fd_host_parent host name: %s\n", *host);
				} else {
					debug_log("[-] converge_fd_host_parent column name: %s\n", 
							sqlite3_column_name(stmt, col));
					exit(248);
				}
			}   
			break;
		} else if (ret == SQLITE_DONE) {
			break;
		} else if (ret != SQLITE_BUSY) {
			debug_log("[-] converge_fd_host_parent step: %s\n", sqlite3_errmsg(db));
			exit(248);
		}
	}

	ret = sqlite3_finalize(stmt);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_fd_host_parent finalize: %s\n", sqlite3_errmsg(db));
		exit(248);
	}
}

void converge_db_clear(pid_t pid, pid_t grp) {
	sqlite3_stmt *stmt;
	// We delete by grp because fork'd processes wont run converge_init, so 
	// possibility of old rows being hit, which is very bad
	char *clear_connect = "DELETE FROM connect WHERE grp = ?";
	char *clear_getaddrinfo = "DELETE FROM getaddrinfo WHERE grp = ?";
	int ret;

	ret = sqlite3_prepare_v2(db, clear_connect, -1, &stmt, NULL);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_db_clear prepare: %s\n", sqlite3_errmsg(db));
		exit(249);
	}

	ret = sqlite3_bind_int(stmt, 1, pid);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_db_clear bind pid: %s\n", sqlite3_errmsg(db));
		exit(249);
	}

	ret = sqlite3_step(stmt);

	if (ret != SQLITE_DONE) {
		debug_log("[-] converge_db_clear step: %s\n", sqlite3_errmsg(db));
		exit(249);
	}

	ret = sqlite3_finalize(stmt);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_db_clear finalize: %s\n", sqlite3_errmsg(db));
		exit(249);
	}

	ret = sqlite3_prepare_v2(db, clear_getaddrinfo, -1, &stmt, NULL);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_db_clear prepare: %s\n", sqlite3_errmsg(db));
		exit(249);
	}

	ret = sqlite3_bind_int(stmt, 1, pid);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_db_clear bind pid: %s\n", sqlite3_errmsg(db));
		exit(249);
	}

	ret = sqlite3_step(stmt);

	if (ret != SQLITE_DONE) {
		debug_log("[-] converge_db_clear step: %s\n", sqlite3_errmsg(db));
		exit(249);
	}

	ret = sqlite3_finalize(stmt);

	if (ret != SQLITE_OK) {
		debug_log("[-] converge_db_clear finalize: %s\n", sqlite3_errmsg(db));
		exit(249);
	}
}

char **copy_env() {
	char **env;
	unsigned int count = 0, i;

	for (env = environ; *env; env++)
		count++;

	// One more for final NULL*
	count++;

	env = xmalloc(count * sizeof(char *));

	for (i = 0; environ[i]; i++) {
		count = strlen(environ[i]) + 1;
		env[i] = xmalloc(count);
		strncpy(env[i], environ[i], count);
	}

	env[i] = NULL;

	return env;
}

void free_env(char **env) {
	char **p;

	for (p = env; *p ; p++)
		free(*p);

	free(env);
}

void remove_shim(char **env) {
	char **e, *ptr, *buf;
	unsigned int shim_len, env_len, diff;

	for (e = env; *e; e++) {
		ptr = strstr(*e, "LD_PRELOAD");
		if (!ptr)
			continue;
		// If LD_PRELOAD is at the start of the string it's the env element
		// we're looking for
		if (!(*e - ptr))
			break;
	}

	ptr = strstr(*e, SHIM_NAME);

	// No shim in LD_PRELOAD, what are we doing here?
	// This is some inception-esque code if you think about it
	if (!ptr)
		return;

	env_len = strlen(*e);
	shim_len = strlen(SHIM_NAME);

	// If the character followingthe shim is a space, we want to remove it
	// as well so we don't end up with 'LD_PRELOAD= foo.so'
	if (*(ptr + shim_len) == ' ')
		shim_len++;

	// If we have '/foo/bar/shim.so', we need to remove '/foo/bar' too
	while (!(*(ptr - 1) == '=' || *(ptr - 1) == ' ')) {
		ptr--;
		shim_len++;
	}

	// Shitty pointer math
	diff = (*e+env_len - ptr - shim_len) + 1; //End of env - end of buf

	// We need another buf to avoid undef behavior with overlapping
	// buffers
	buf = xmalloc(diff);

	// Copy from the end of the shim until the end of the env entr
	strncpy(buf, ptr + shim_len, diff);
	// Copy it all back to the start of the shim
	strncpy(ptr, buf, diff);

	free(buf);
}
//*/
